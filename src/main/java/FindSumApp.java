import java.util.StringTokenizer;

     class FindSumApp {
        private static String delim = " ";

        public static void findSum(int[] input, int target, String slicedString, int index) {
            if(index > (input.length - 1)) {
                if(calculatetSum(slicedString) == target && index != 1) {
                    System.out.println(slicedString);
                    return;
                }
                return;
            }
            findSum(input, target, slicedString + input[index] + delim, index + 1);
            findSum(input, target, slicedString, index + 1);
        }
        private static int calculatetSum(String slicedString) {
            int sum = 0;
            StringTokenizer eTokens = new StringTokenizer(slicedString, delim);
            while (eTokens.hasMoreElements()) {
                sum += Integer.parseInt((String) eTokens.nextElement());
            }
            return sum;
        }
    }

